#ifndef _STACK_H_
#define _STACK_H_

#include <stdexcept>
#include <algorithm>

template <typename T>
class Node {
public:
    Node<T>* next;
    T data;

    Node(Node<T>* pnode = nullptr, T pdata = 0): next(pnode), data(pdata) {}
};

template <typename T>
class Stack {
private:
    Node<T>* top = nullptr;
    int _size = 0;
public:
    Stack() {}
    ~Stack();
    void push (T data);

    T pop();

    int size() { return _size; }
    bool empty() {return _size==0;}
};
template <typename T>
T Stack<T>::pop() {
  if (empty()) {
    throw std::range_error{"Can't pop empty stack"};
  } else {
    T topData = top->data;
    Node<T> *oldTop = top;
    top = top->next;
    delete oldTop;
    _size--;
    return topData;
  }
}
template <typename T>
void Stack<T>::push(T data) {
  Node<T>* newTop = new Node<T>(top,data);
  top = newTop;
  _size++;
}
template <typename T>
Stack<T>::~Stack() {
  while(!empty()) {
    Node<T> *oldTop = top;
    top = top->next;
    delete oldTop;
    _size--;
  }
}


#endif